package com.trossky.dev.utils;



/*
 * Copyright (c) 2017. Trossky Developer.
 *  CEO Luis Fernando Garcia.
 */

public class K {


    public static final String GOLANG="GOLANG";
    public static final String HELLO="HELLO";
    public static final String EXPR="EXPR";
    public static final String ARRAY_INIT="ArrayInit";
    public static final String MYLANGUAGE="MYLANGUAGE";
    public static final String OTHER="OTHER";


    public static final String DEFINE="DEFINE";
    public static final String REFERENCE="REFERENCE";
    public static final String ATTRIB="ATTRIB";


}
