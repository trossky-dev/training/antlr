package com.trossky.dev;


import com.trossky.dev.talp.Talp;
import com.trossky.dev.tuto.Tuto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.trossky.dev.utils.K.*;

public class Main {
    public static void main(String[] args) {
        try {
            boolean isTalp=false;
            boolean isTuto=true;

            // Declarations
            Tuto tuto= new Tuto();
            Talp talp= new Talp();
            List<String> inputs= new ArrayList<>();
            HashMap<String,List<String>> parameters= new HashMap<>();


            // Run from class
            if (isTalp){

                inputs=new ArrayList<>();
                parameters.put(HELLO,inputs);
                inputs.add("hello.txt");
                talp.beginTalp(parameters);

                inputs=new ArrayList<>();
                parameters.put(EXPR,inputs);
                inputs.add("expr.txt");
                talp.beginTalp(parameters);

                inputs=new ArrayList<>();
                parameters.put(ARRAY_INIT,inputs);
                inputs.add("arrayinit.txt");
                talp.beginTalp(parameters);

                inputs=new ArrayList<>();
                parameters.put(MYLANGUAGE,inputs);
                inputs.add("myLanguage.txt");
                talp.beginTalp(parameters);

            }



            if (isTuto){

                // Run from tutorials
                inputs=new ArrayList<>();
                //inputs.add("golang/example.go");
                //inputs.add("golang/ill_but_correct.go");
                for (int i = 1; i <2; i++) {
                    if (i<10){
                        inputs.add("golang/0"+i+"_case.go");
                    }else{
                        inputs.add("golang/"+i+"_case.go");
                    }

                }
                parameters.put(GOLANG,inputs);
                tuto.beginTuto(parameters);


            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
