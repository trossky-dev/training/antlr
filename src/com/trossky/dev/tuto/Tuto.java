package com.trossky.dev.tuto;


import com.trossky.dev.tuto.golang.AlgorithmLVA;
import com.trossky.dev.tuto.golang.AnalyzerGolang;
import com.trossky.dev.tuto.golang.antlr.GolangLexer;
import com.trossky.dev.tuto.golang.antlr.GolangParser;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.*;
import java.util.HashMap;
import java.util.List;

import static com.trossky.dev.tuto.golang.AlgorithmLVA.lineRemove;
import static com.trossky.dev.utils.K.GOLANG;

/**
 * Created by luis on 30/04/17.
 */
public class Tuto {

    public void beginTuto(HashMap<String, List<String>> parameters){

        parameters.forEach((nameTuto, inputs) -> {
           if (nameTuto.equals(GOLANG)){
               for (String input:inputs) {
                   golang(input);
               }


           }

        });




    }

    private void golang(String args) {

        try {




            System.out.println("\n");
            System.out.println("**************************************************************");
            System.out.println("*****************       ***********   **  *******************");
            System.out.println("****************  ****************  ******  ******************");
            System.out.println("***************  ****************  ********  *****************");
            System.out.println("***************  *****    *******  ********  *****************");
            System.out.println("***************  ********  *******  ******  ******************");
            System.out.println("****************  *****  ***********  ***  *******************");
            System.out.println("******************     ***************   *********************");
            System.out.println("**************************************************************");

            //Crea el analizador lexico que se alimentara a partir de la entrada (archivo o consola)

            GolangLexer golangLexer;

            if (args.length()>0)
                golangLexer= new GolangLexer(new ANTLRFileStream(args));
            else
                golangLexer= new GolangLexer(new ANTLRInputStream(System.in));

            //Identificar el analizador lexico como fuente de tokens para el sintactico
            CommonTokenStream tokenStream = new CommonTokenStream(golangLexer);

            //Crear el analizador sintactico que se alimenta a partir del buffer de tokens

            GolangParser golangParser= new GolangParser(tokenStream);

            GolangParser.SourceFileContext parseTree= golangParser.sourceFile();// Comienza el analisis en la regla inicial

            System.out.println(parseTree.toStringTree(golangParser));
            System.out.println("\n");
            AnalyzerGolang<Object> loader = new AnalyzerGolang<Object>();
            loader.visit(parseTree);

            AlgorithmLVA algorithmLVA= new AlgorithmLVA();
            algorithmLVA.beginAnalizerLVA();

            createFile(args);












        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error (TEST) ->  : "+e);
        }

    }

    public  void createFile(String args) throws IOException {


        FileReader fr = null;
        BufferedReader br = null;
        int n=1;


        String ruta = "./output.go";
        File archivo = new File(args);
        File output = new File(ruta);
        BufferedWriter bw;

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File (args);
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            bw = new BufferedWriter(new FileWriter(output));

            // Lectura del fichero
            String linea;
            while((linea=br.readLine())!=null) {
                System.out.println(linea);

                int i = lineRemove.indexOf(n);
                if (i<0){
                    bw.write(linea);
                    bw.write("\n");
                }

                n+=1;

            }




            bw.close();

        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta
            // una excepcion.
            try{
                if( null != fr ){
                    fr.close();
                }
            }catch (Exception e2){
                e2.printStackTrace();
            }
        }

    }

}
