package com.trossky.dev.talp;

import com.trossky.dev.talp.arrayInit.ArrayInitLexer;
import com.trossky.dev.talp.arrayInit.ArrayInitParser;
import com.trossky.dev.talp.arrayInit.ShortToUnicodeString;
import com.trossky.dev.talp.expr.ExprLexer;
import com.trossky.dev.talp.expr.ExprParser;
import com.trossky.dev.talp.hello.HelloLexer;
import com.trossky.dev.talp.hello.HelloParser;
import com.trossky.dev.talp.myLanguage.MyVisitor;
import com.trossky.dev.talp.myLanguage.fromAntlr.MyLanguageLexer;
import com.trossky.dev.talp.myLanguage.fromAntlr.MyLanguageParser;
import org.antlr.runtime.tree.ParseTree;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.HashMap;
import java.util.List;

import static com.trossky.dev.utils.K.*;


/*
 * Copyright (c) 2017. Trossky Developer CEO Luis Fernando Garcia.
 */

public class Talp {
    public void beginTalp(HashMap<String, List<String>> parameters){

        parameters.forEach((nameTalp, inputs) -> {
            if (nameTalp.equals(HELLO)){
                for (String input:inputs) {
                    hello(input);
                }
            }
            if (nameTalp.equals(EXPR)){
                for (String input:inputs){
                    expr(input);
                }
            }
            if (nameTalp.equals(ARRAY_INIT)){
                for (String input: inputs){
                    arrayInit(input);

                }
            }
            if (nameTalp.equals( MYLANGUAGE)){
                for (String input: inputs){
                    myLanguage(input);

                }
            }

        });




    }



    public void hello(String args){


            try {
                System.out.println("\n");
                System.out.println("******************************************************************");
                System.out.println("******  ******  ***       ***  *******  **********   **  *****");
                System.out.println("******  ******  ***  ********  *******  *********  *****  ****");
                System.out.println("******  ******  ***  ********  *******  *******  ********  ***");
                System.out.println("******          ***      ****  *******  *******  ********  ***");
                System.out.println("******  ******  ***  ********  *******  ********  ******  ****");
                System.out.println("******  ******  ***  ********  *******  **********  ***  *****");
                System.out.println("******  ******  ***       ***       **       ******   *******");
                System.out.println("******************************************************************");

                //Crea el analizador lexico que se alimentara a partir de la entrada (archivo o consola)

                HelloLexer helloLexer;

                if (args.length()>0)
                    helloLexer= new HelloLexer(new ANTLRFileStream(args));
                else
                    helloLexer= new HelloLexer(new ANTLRInputStream(System.in));

                //Identificar el analizador lexico como fuente de tokens para el sintactico
                CommonTokenStream tokenStream = new CommonTokenStream(helloLexer);

                //Crear el analizador sintactico que se alimenta a partir del buffer de tokens

                HelloParser helloParser= new HelloParser(tokenStream);

                HelloParser.RContext parseTree= helloParser.r();// Comienza el analisis en la regla inicial

                System.out.println(parseTree.toStringTree(helloParser));
                System.out.println("\n");


            }catch (Exception e){
                e.printStackTrace();
                System.out.println("Error (TEST) ->  : "+e);
            }
        }

    public void expr(String args){


        try {
            System.out.println("\n");
            System.out.println("****************************************************************");
            System.out.println("*********       ***  *******   **  ******  ***  ******   ***");
            System.out.println("*********  *********  *****  ****  ******  ***  ******   ***");
            System.out.println("*********  **********  **  ******  ******  ***  ******   ***");
            System.out.println("*********      ******  **  ******         ****         ****");
            System.out.println("*********  *********  ****  *****  ***********  *  ********");
            System.out.println("*********  ********  *******  ***  ***********  **  *******");
            System.out.println("*********       * ***********  **  ***********  ***  ******");
            System.out.println("****************************************************************");

            //Crea el analizador lexico que se alimentara a partir de la entrada (archivo o consola)

            ExprLexer exprLexer;

            if (args.length()>0)
                exprLexer= new ExprLexer(new ANTLRFileStream(args));
            else
                exprLexer= new ExprLexer(new ANTLRInputStream(System.in));

            //Identificar el analizador lexico como fuente de tokens para el sintactico
            CommonTokenStream tokenStream = new CommonTokenStream(exprLexer);

            //Crear el analizador sintactico que se alimenta a partir del buffer de tokens

            ExprParser exprParser= new ExprParser(tokenStream);

            ExprParser.ExprContext parseTree= exprParser.expr();// Comienza el analisis en la regla inicial

            System.out.println(parseTree.toStringTree(exprParser));
            System.out.println("\n");


        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error (TEST) ->  : "+e);
        }
    }

    private void arrayInit(String args) {
        try {

            System.out.println("\n");
            System.out.println("****************************************************************");
            System.out.println("\t\t Begin drawn tree \n");
            System.out.println("****************************************************************");
            ArrayInitLexer arrayInitLexer;
            if (args.length()>0){
                arrayInitLexer= new ArrayInitLexer(new ANTLRFileStream(args));
            }else{
                arrayInitLexer= new ArrayInitLexer(new ANTLRInputStream(System.in));
            }

            CommonTokenStream tokens= new CommonTokenStream(arrayInitLexer);

            // create a aprser that feeds off the tokens buffer
            ArrayInitParser parcer= new ArrayInitParser(tokens);
            ArrayInitParser.InitContext tree=parcer.init();

            // see  the tree parcer
            System.out.println(tree.toStringTree(parcer));


            // NOW: use the Walker

            // Create a generic parse tree walker thar can trigger callbacks
            ParseTreeWalker walker= new ParseTreeWalker();

            // Walke the tree created during the parse, trigger callbacks

            walker.walk(new ShortToUnicodeString(),tree);
            System.out.println("\nTraduccion Finalizada");





        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error (TEST) ->  : "+e);
        }

    }

    private void myLanguage(String args) {
        try {

            System.out.println("\n");
            System.out.println("****************************************************************");
            System.out.println("\t\t Begin Interpreter My Language \n");
            System.out.println("****************************************************************");

            MyLanguageLexer myLanguageLexer;

            if (args.length()>0){
                myLanguageLexer= new MyLanguageLexer(new ANTLRFileStream(args));
            }else{
                myLanguageLexer= new MyLanguageLexer(new ANTLRInputStream(System.in));
            }

            CommonTokenStream tokens= new CommonTokenStream(myLanguageLexer);

            // create a aprser that feeds off the tokens buffer
            MyLanguageParser parcer= new MyLanguageParser(tokens);
            MyLanguageParser.CommandsContext tree=parcer.commands();

            // see  the tree parcer
            System.out.println(tree.toStringTree(parcer));

            MyVisitor<Object> loader = new MyVisitor<Object>();
            loader.visit(tree);
            System.out.println("\nMyLanguage finished");





        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error (TEST) ->  : "+e);
        }

    }

}
