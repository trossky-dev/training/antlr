package com.trossky.dev.talp.myLanguage;

import com.trossky.dev.talp.myLanguage.fromAntlr.MyLanguageBaseVisitor;
import com.trossky.dev.talp.myLanguage.fromAntlr.MyLanguageParser;

import java.util.HashMap;


/*
 * Copyright (c) 2017. Trossky Developer.
 *  CEO Luis Fernando Garcia.
 */

public class MyVisitor<T> extends MyLanguageBaseVisitor<T> {
    public HashMap<String,Object> symbolTable= new HashMap<>();

    @Override
    public T visitRepeat(MyLanguageParser.RepeatContext ctx) {
        // Obtener de la exprecion el NUMERO(times)  de iteraciones que el ciclo debe realizar
        int times= (int)(double)(Double) visitExpr(ctx.expr());

        // Creamos un ciclo hasta el Numero de Iteraciones obtenidas
        for (int i = 0; i < times; i++) {

            // Ejecutamos los camandos ((times)) veces. Que tiene el nodo hijo de esta regla de repeat.
            visitCommands(ctx.commands());
        }
        return null;
    }


    @Override
    public T visitConditional(MyLanguageParser.ConditionalContext ctx) {
        String op=ctx.ROP().getText();
        Double num1=(Double) visitExpr(ctx.expr(0));
        Double num2=(Double) visitExpr(ctx.expr(1));

        Boolean ans=null;

        switch (op){
            case "<":
                ans=num1<num2;
                break;
            case "<=":
                ans=num1<=num2;
                break;
            case ">=":
                ans=num1>=num2;
                break;
            case ">":
                ans=num1>num2;
                break;
            case "==":
                ans=num1==num2;
                break;
            case "!=":
                ans=num1!=num2;
                break;
            default:
                System.out.println("Operador Desconocido! -> "+op);
                break;
        }
        if (ans){
            visitCommands(ctx.commands());
        }
        return null;
    }

    @Override
    public T visitCommand(MyLanguageParser.CommandContext ctx) {
        // si el comando es una imprecion por pantalla.
        if (ctx.printexpr()!=null){
            Double ans=(Double) visitExpr(ctx.printexpr().expr()); // obtenemos el numero a imprimir
            int aux=(int) Math.floor(ans);

            if (ans-aux<1e-9){ // verificamos si se puede imprimir como entero

                System.out.println(aux); // imprimimos su equivalente ENTERO de los valores decimales 1.00

            }else{
                System.out.println(ans); // imprimimos el valor decimal
            }


            // O si es una declaracion de variable en el comando
        }else if (ctx.VAR()!=null){
            String name= ctx.ID().getText();

            // Si ya existe esta variable, no deberia existir otra declaracion. Por ende es un error sintactico.
            if (symbolTable.get(name)!=null){
                errorSemanticDoubleDeclarationField(ctx);
                //Otenemos la linea, donde se declara la variable con su respectivo ID.


            }else {
                symbolTable.put(name,visitExpr(ctx.expr()));
            }

        }else {
            return visitChildren(ctx);
        }

        return null;
    }

    private void errorSemanticDoubleDeclarationField(MyLanguageParser.CommandContext ctx) {

        int line =ctx.ID().getSymbol().getLine();
        int col= ctx.ID().getSymbol().getCharPositionInLine()+1;

        System.err.printf("<%d.%d> Error Semantico, la Variable con nombre:  \""+ctx.ID().getText()+"\" ya fue declarada. \n",line,col);
        System.exit(-1);
    }


    @Override
    public T visitExpr(MyLanguageParser.ExprContext ctx) {
        // Si la expr es un decimal
        if (ctx.DOUBLE()!=null){
            Double num= new Double(ctx.DOUBLE().getText());

            System.out.println("Valor Decimal de la [EXPR] -> "+ num );
            return  (T)num;

        }else  if (ctx.PIZQ()!=null){
            return  visitExpr(ctx.expr(0));
        }else if (ctx.ID()!=null){
            String name = ctx.ID().getText();
            Object value;
            //verificar en la tabla de simbulos si ya existe, entonces generar un error semantico.

            if ((value=symbolTable.get(name))==null){
                int line =ctx.ID().getSymbol().getLine();
                int col= ctx.ID().getSymbol().getCharPositionInLine()+1;

                System.err.printf("<%d.%d> Error Semantico, la Variable con nombre:  \""+ctx.ID().getText()+"\" no fue declarada. \n",line,col);
                System.exit(-1);

                return null;
            }else{
                return (T)value;
            }
        }else{
            String op =(ctx.MULOP()!=null? ctx.MULOP().getText():ctx.SUMOP().getText());
            Double num1 = (Double) visitExpr(ctx.expr(0));
            Double num2 = (Double) visitExpr(ctx.expr(1));
            Double ans = null;


            switch (op){

                case  "*":
                    ans=num1*num2;
                    break;
                case "-":
                    ans=num1-num2;
                    break;
                case "+":
                    ans=num1+num2;
                    break;
                case  "/":
                    ans=num1/num2;
                    break;
                default:
                    System.out.println("Operador desconocido");
                    break;
            }

            return (T)(Double) ans;
        }
    }
}
