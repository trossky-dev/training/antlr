package com.trossky.dev.talp.arrayInit;

/**
 * Created by luis on 2/05/17.
 */
public class ShortToUnicodeString extends ArrayInitBaseListener {

    @Override /* Tanslate { to " */
    public void enterInit(ArrayInitParser.InitContext ctx) {

        System.out.print('"');
    }

    @Override /* Tanslate } to " */
    public void exitInit(ArrayInitParser.InitContext ctx) {

        System.out.print('"');
    }

    @Override  /*translate intege to 4.digit hexadecimal strings prefixed with \\u */
    public void enterValue(ArrayInitParser.ValueContext ctx) {
        //System.out.println("accediendo a los elementos del array ->"+ctx.toStringTree());
        int value= Integer.valueOf(ctx.INT().getText());
        System.out.printf("\\u%04x",value);

    }



}
